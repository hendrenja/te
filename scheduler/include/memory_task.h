#ifndef __SCHEDULER_MEMORY_TASK_BASE_H__
#define __SCHEDULER_MEMORY_TASK_BASE_H__

#include <corto/corto.h>
#include <include/task_base.h>
#include <string>

class CMemoryTask : public CTaskBase
{
public:
    bool Initialize(std::string name);
    bool Execute();

    CMemoryTask();
    virtual ~CMemoryTask();
};

#endif
