#ifndef __SCHEDULER_TASK_BASE_H__
#define __SCHEDULER_TASK_BASE_H__

#include <string>

class CTaskBase
{
public:
    virtual bool Initialize(std::string name) = 0;
    virtual bool Execute() = 0;

    std::string Name(void);
    void Name(std::string name);

    CTaskBase();
    virtual ~CTaskBase();

private:
    std::string     m_name;
};

#endif
