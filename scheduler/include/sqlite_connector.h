#ifndef __SCHEDULER_SQLITE_CONNECTOR_H__
#define __SCHEDULER_SQLITE_CONNECTOR_H__

#include <corto/corto.h>
#include <oasys/core/common/singleton.h>
#include <string>
#include <mutex>
#include <sqlite3/sqlite3.h>

class CSqliteConnector
{
public:
    bool Initialize(std::string &dbName);
    bool Initialized(void);
    bool Execute();

private:
    CSqliteConnector();
    virtual ~CSqliteConnector();

    std::mutex      m_mutex;
    sqlite3         *m_pDatabase;
    sig_atomic_t    m_initialized;

    friend class oasys::core::common::TSingleton<CSqliteConnector>;
};

typedef oasys::core::common::TSingleton<CSqliteConnector> SqliteConnector;

#endif
