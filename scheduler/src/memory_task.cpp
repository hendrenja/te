#include <include/memory_task.h>

bool CMemoryTask::Initialize(std::string name)
{
    Name(name);
    return true;
}

bool CMemoryTask::Execute()
{
    corto_info("Running [%s]", Name().c_str());
    return true;
}

CMemoryTask::CMemoryTask()
{

}

CMemoryTask::~CMemoryTask()
{

}
