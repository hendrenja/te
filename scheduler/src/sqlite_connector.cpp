#include <include/sqlite_connector.h>

bool CSqliteConnector::Initialize(std::string &dbName)
{
    if (m_pDatabase == nullptr)
    {
        if (sqlite3_open(dbName.c_str(), &m_pDatabase) != 0)
        {
            corto_error("Failed to open SQLITE Database [%s]", dbName.c_str());
            return false;
        }
        if (m_pDatabase == nullptr)
        {
            corto_error("Failed to open SQLITE Database [%s]", dbName.c_str());
            return false;
        }
    }

    return true;
}

bool CSqliteConnector::Execute()
{
    return true;
}

CSqliteConnector::CSqliteConnector() :
    m_pDatabase(nullptr)
{

}

CSqliteConnector::~CSqliteConnector()
{

}
