#include <include/scheduler.h>
#include <include/memory_task.h>
#include <include/sqlite_connector.h>

const char * CONFIG_MEMORY_KEY = "memory";
const char * CONFIG_TCP_KEY = "tcp";
const char * CONFIG_SQLITE_DB_KEY = "db";
const char * CONFIG_TCP_TARGET_KEY = "target";
const char * CONFIG_FREQUENCY_KEY = "frequency";

uint64_t MEMORY_TASK_FREQUENCY = 2;
uint64_t NET_TASK_FREQUENCY = 8;
std::string TCP_TARGET = "";
std::string DB_NAME = "";

#define JSON_VALUE_FREE(ptr) if (ptr != nullptr) { json_value_free(ptr); ptr = nullptr;}
#define JSON_VERIFY_PTR(ptr, name) if (ptr == nullptr) { \
            corto_error("Failed JSON access - Invalid pointer [%s]", name); return false;}


bool UpdateConfig(std::string &file)
{
    JSON_Value *pFile = json_parse_file(file.c_str());
    JSON_VERIFY_PTR(pFile, "pFile");

    JSON_Object *pFileObj = json_value_get_object(pFile);
    JSON_VERIFY_PTR(pFileObj, "pFileObj");

    JSON_Array *pMemoryArr = json_object_get_array(pFileObj, CONFIG_MEMORY_KEY);
    JSON_VERIFY_PTR(pMemoryArr, "pMemoryArr");

    JSON_Array *pTcpArr = json_object_get_array(pFileObj, CONFIG_TCP_KEY);
    JSON_VERIFY_PTR(pTcpArr, "pTcpArr");

    JSON_Object *pMemoryObj  = json_array_get_object(pMemoryArr, 0);
    JSON_VERIFY_PTR(pMemoryObj, "pMemoryObj");

    JSON_Object *pTcpObj  = json_array_get_object(pTcpArr, 0);
    JSON_VERIFY_PTR(pTcpObj, "pTcpObj");

    double memFrequency = json_object_get_number(pMemoryObj, CONFIG_FREQUENCY_KEY);
    if (memFrequency != 0)
    {
        MEMORY_TASK_FREQUENCY = (uint64_t)memFrequency;
        corto_info("Execute memory task [%ul]s", MEMORY_TASK_FREQUENCY);
    }
    else
    {
        corto_error("Failed to configure memory task frequency.");
        return false;
    }

    double tcpFrequency = json_object_get_number(pTcpObj, CONFIG_FREQUENCY_KEY);
    if (tcpFrequency != 0)
    {
        NET_TASK_FREQUENCY = tcpFrequency;
        corto_info("Execute TCP task [%u]s", NET_TASK_FREQUENCY);
    }
    else
    {
        corto_error("Failed to configure net task frequency.");
        return false;
    }

    const char * tcpTarget = json_object_get_string(pTcpObj, CONFIG_TCP_TARGET_KEY);
    if (tcpTarget != nullptr)
    {
        TCP_TARGET.assign(tcpTarget);
        corto_info("TCP Task Target [%s]", TCP_TARGET.c_str());
    }
    else
    {
        corto_error("Failed to configure net task target.");
        return false;
    }

    const char * dbName = json_object_get_string(pFileObj, CONFIG_SQLITE_DB_KEY);
    if (dbName != nullptr)
    {
        DB_NAME.assign(dbName);
        corto_info("SQLITE Database [%s]", DB_NAME.c_str());
    }
    else
    {
        corto_error("Failed to configure database name.");
        return false;
    }

    JSON_VALUE_FREE(pFile);

    return true;
}

int schedulerMain(int argc, char *argv[])
{
    CScheduler scheduler;
    scheduler.Initialize();

    std::shared_ptr<CMemoryTask> pTask1 = std::make_shared<CMemoryTask>();
    pTask1->Initialize("Memory 1");

    std::shared_ptr<CMemoryTask> pTask2 = std::make_shared<CMemoryTask>();
    pTask2->Initialize("Memory2");

    if (argc > 1)
    {
        std::string file(argv[1]);
        if (UpdateConfig(file) == false)
        {
            corto_error("Failed to configure.");
            return 1;
        }
    }
    else
    {
        corto_error("No configuration file specified.");
        corto_info("Usage: ./scheduler <config.json>");
        return 1;
    }

    if (SqliteConnector::GetInstance()->Initialize(DB_NAME) == false)
    {
        corto_error("Failed to configure SQLITE Connector.");
        return 1;
    }

    scheduler.AddTaskSeconds(pTask1, MEMORY_TASK_FREQUENCY);
    scheduler.AddTaskSeconds(pTask2, NET_TASK_FREQUENCY);

    std::this_thread::sleep_for(std::chrono::milliseconds(5 * 1000));

    if (pTask2.get() != nullptr)
    {
        pTask2.reset();
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(5 * 1000));
    scheduler.RemoveTask("Memory 1");
    scheduler.RemoveTask("Memory2");

    return 0;
}
