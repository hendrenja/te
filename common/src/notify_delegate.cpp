#include <oasys/core/common/notify_delegate.h>

using namespace oasys::core::common;

bool CNotifyDelegate::Valid()
{
    bool retVal = false;

    if ((m_pOwner.use_count() > 0) && (m_callback != nullptr))
    {
        retVal = true;
    }

    return retVal;
}

bool CNotifyDelegate::Equal(OwnerWeakPtr comparePtr)
{
    return !m_pOwner.owner_before(comparePtr)
            && !comparePtr.owner_before(m_pOwner);
}

OwnerWeakPtr CNotifyDelegate::GetOwnerPtr()
{
    return m_pOwner;
}

typename CNotifyDelegate::Callback& CNotifyDelegate::GetCallback()
{
    return m_callback;
}

void CNotifyDelegate::SetCallback(Callback &callback)
{
    m_callback = callback;
}

bool CNotifyDelegate::Execute()
{
    bool retVal = false;

    retVal = Valid();
    if (retVal == true)
    {
        m_callback();
    }

    return retVal;
}

CNotifyDelegate::CNotifyDelegate(const Callback &callback,
                                 OwnerSharedPtr pOwner) :
    m_pOwner(pOwner),
    m_callback(std::move(callback))
{
}

CNotifyDelegate::~CNotifyDelegate()
{
}
