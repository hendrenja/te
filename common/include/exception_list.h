#ifndef __OASYS_CORE_COMMON_EXCEPTION_LIST_H__
#define __OASYS_CORE_COMMON_EXCEPTION_LIST_H__

#include <oasys/core/common/exception.h>

namespace oasys
{
namespace core
{
namespace common
{

/**
 * Exception class declarations
 */
class CConnextDdsException {};

/**
 * Exception typedef definitions
 */
typedef TException<CConnextDdsException> OASYS_DDS_CONNEXT_EXCEPTION;

}
}
}


#endif // __OASYS_CORE_COMMON_EXCEPTION_LIST_H__
