#ifndef __OASYS_CORE_COMMON_NOTIFY_DELEGATE_H__
#define __OASYS_CORE_COMMON_NOTIFY_DELEGATE_H__

#include <functional>
#include <memory>

namespace oasys
{
namespace core
{
namespace common
{

#ifndef __OASYS_CORE_COMMON_OWNER_PTR__
#define __OASYS_CORE_COMMON_OWNER_PTR__
typedef std::shared_ptr<void> OwnerSharedPtr;
typedef std::weak_ptr<void> OwnerWeakPtr;
#endif

class CNotifyDelegate
{
public:
    typedef std::function<void()> Callback;
    bool Valid();
    bool Equal(OwnerWeakPtr comparePtr);
    OwnerWeakPtr GetOwnerPtr();
    Callback &GetCallback();
    void SetCallback(Callback &callback);

    bool Execute();

    CNotifyDelegate(const Callback &callback,
                    OwnerSharedPtr pOwner);
    virtual ~CNotifyDelegate();

private:
    OwnerWeakPtr   m_pOwner;
    Callback       m_callback;
};

}
}
}

#endif //__OASYS_CORE_COMMON_NOTIFY_DELEGATE_H__
