#ifndef __OASYS_CORE_COMMON_SINGLETON_H__
#define __OASYS_CORE_COMMON_SINGLETON_H__

#include <atomic>
#include <memory>
#include <mutex>

namespace oasys
{
namespace core
{
namespace common
{

template <class T>
class TSingleton
{
public:
    static T *GetInstance();
    static bool Destroy();

private:
    TSingleton(void)= default;
    ~TSingleton(void)= default;

    TSingleton(const TSingleton &singleton);
    TSingleton & operator = (const TSingleton &rhs);

    static std::atomic<T*> m_pInstance;
    static std::mutex m_mutex;
};

template <class T>
std::atomic<T*> TSingleton<T>::m_pInstance;
template <class T>
std::mutex TSingleton<T>::m_mutex;

#include <oasys/core/common/singleton.cpp>

}
}
}
#endif //__OASYS_CORE_COMMON_SINGLETON_H__
