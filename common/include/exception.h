#ifndef __OASYS_CORE_COMMON_EXCEPTION_H__
#define __OASYS_CORE_COMMON_EXCEPTION_H__

#include <string>

namespace oasys
{
namespace core
{
namespace common
{

template <class T>
class TException : public std::exception
{
public:
    /** Constructor (C strings).
     */
    explicit TException()
    {
    }

    /** Constructor (C strings).
     *  @param message C-style string error message.
     *                 The string contents are copied upon construction.
     *                 Hence, responsibility for deleting the char* lies
     *                 with the caller.
     */
    explicit TException(const char* message) :
        m_message(message)
    {
    }

    /** Constructor (C++ STL strings).
     *  @param message The error message.
     */
    explicit TException(const std::string& message) :
        m_message(message)
    {
    }

    /** Destructor.
     * Virtual to allow for subclassing.
     */
    virtual ~TException() throw () {}

    /** Returns a pointer to the (constant) error description.
     *  @return A pointer to a const char*. The underlying memory
     *          is in posession of the Exception object. Callers must
     *          not attempt to free the memory.
     */
    virtual const char* what() const throw ()
    {
        return m_message.c_str();
    }

protected:
    /** Error message.
     */
    std::string m_message;
};

}
}
}

#endif // __OASYS_CORE_COMMON_EXCEPTION_H__
