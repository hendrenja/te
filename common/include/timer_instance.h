#ifndef __OASYS_CORE_COMMON_TIMER_INSTANCE_H__
#define __OASYS_CORE_COMMON_TIMER_INSTANCE_H__

#include <algorithm>
#include <chrono>

namespace oasys
{
namespace core
{
namespace common
{

typedef std::chrono::steady_clock Clock;
typedef std::chrono::time_point<Clock> Timestamp;
typedef std::chrono::microseconds Period;
typedef std::chrono::duration<double> Duration;

template <class TCallback, class TId>
class TInstance
{

public :
    TInstance(TId id = 0) :
        m_id(id),
        m_duration(0.0),
        m_active(false),
        m_delete(false),
        m_pWaitFlag(nullptr)
    {
    }

    TInstance(TId id, Timestamp next, Period period, TCallback callback) noexcept :
        m_id(id),
        m_duration(0.0),
        m_next(next),
        m_period(period),
        m_callback(std::forward<TCallback>(callback)),
        m_active(false),
        m_delete(false),
        m_pWaitFlag(nullptr)
    {
    }

    TInstance(TInstance const& r) = delete;

    TInstance(TInstance&& r) noexcept :
        m_id(r.m_id),
        m_duration(r.m_duration),
        m_next(r.m_next),
        m_period(r.m_period),
        m_callback(std::move(r.m_callback)),
        m_active(r.m_active),
        m_delete(r.m_delete),
        m_pWaitFlag(r.m_pWaitFlag)
    {
        m_callback();
    }

    TInstance& operator=(TInstance const& r) = delete;

    TInstance& operator=(TInstance&& r)
    {
        if (this != &r)
        {
            m_id = r.m_id;
            m_duration = r.m_duration;
            m_next = r.m_next;
            m_period = r.m_period;
            m_callback = std::move(r.m_callback);
            m_active = r.m_active;
            m_delete = r.m_delete;
            m_pWaitFlag = r.m_pWaitFlag;
        }
        return *this;
    }

    TId         m_id;
    Duration    m_duration;
    Timestamp   m_next;
    Period      m_period;
    TCallback   m_callback;
    bool        m_active;
    bool        m_delete;
    bool        *m_pWaitFlag;
};

}
}
}
#endif // __OASYS_CORE_COMMON_TIMER_INSTANCE_H__
