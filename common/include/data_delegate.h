#ifndef __OASYS_CORE_COMMON_DELEGATE_H__
#define __OASYS_CORE_COMMON_DELEGATE_H__

#include <functional>
#include <memory>

namespace oasys
{
namespace core
{
namespace common
{

#ifndef __OASYS_CORE_COMMON_OWNER_PTR__
#define __OASYS_CORE_COMMON_OWNER_PTR__
typedef std::shared_ptr<void> OwnerSharedPtr;
typedef std::weak_ptr<void> OwnerWeakPtr;
#endif

template <typename... Data>
class TDataDelegate
{
public:
    typedef std::function<void(Data...)> Callback;
    bool Valid();
    bool Equal(OwnerWeakPtr comparePtr);
    OwnerWeakPtr GetOwnerPtr();
    Callback &GetCallback();
    void SetCallback(Callback &callback);

    bool Execute(Data&... data);

    TDataDelegate(const Callback &callback,
              OwnerSharedPtr pOwner);
    virtual ~TDataDelegate();

private:
    OwnerWeakPtr   m_pOwner;
    Callback       m_callback;
};

}
}
}

#include "data_delegate.cpp"

#endif //__OASYS_CORE_COMMON_DELEGATE_H__
