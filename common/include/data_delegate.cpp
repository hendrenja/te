#include <oasys/core/common/data_delegate.h>

using namespace oasys::core::common;

template <typename... Data>
bool TDataDelegate<Data...>::Valid()
{
    bool retVal = false;

    if ((m_pOwner.use_count() > 0) && (m_callback != nullptr))
    {
        retVal = true;
    }

    return retVal;
}

template <typename... Data>
bool TDataDelegate<Data...>::Equal(OwnerWeakPtr comparePtr)
{
    return !m_pOwner.owner_before(comparePtr)
            && !comparePtr.owner_before(m_pOwner);
}

template <typename... Data>
OwnerWeakPtr TDataDelegate<Data...>::GetOwnerPtr()
{
    return m_pOwner;
}

template <typename... Data>
typename TDataDelegate<Data...>::Callback& TDataDelegate<Data...>::GetCallback()
{
    return m_callback;
}

template <typename... Data>
void TDataDelegate<Data...>::SetCallback(Callback &callback)
{
    m_callback = callback;
}

template <typename... Data>
bool TDataDelegate<Data...>::Execute(Data&... data)
{
    bool retVal = false;

    retVal = Valid();
    if (retVal == true)
    {
        m_callback(data...);
    }

    return retVal;
}

template <typename... Data>
TDataDelegate<Data...>::TDataDelegate(const Callback &callback,
                              OwnerSharedPtr pOwner) :
    m_pOwner(pOwner),
    m_callback(std::move(callback))
{
}

template <typename... Data>
TDataDelegate<Data...>::~TDataDelegate()
{
}
