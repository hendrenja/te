#ifndef __OASYS_CORE_COMMON_TIMER_H__
#define __OASYS_CORE_COMMON_TIMER_H__

#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <map>
#include <set>
#include <cstdint>
#include <signal.h>
#include <oasys/core/common/timer_instance.h>

namespace oasys
{
namespace core
{
namespace common
{

/**
 * @brief Comparison functor used to sort the timer queue
 * @details Queue is a multiset that requires a custom comparator for
 * TimerInstance nodes. Nodes are compared by their `next` member.
 */
template <typename TInstance>
struct SetComparator
{
    bool operator()(const TInstance *a, const TInstance *b) const
    {
        return a->m_next < b->m_next;
    }
};

class CTimer
{
public:
    typedef uint64_t TimerId;
    typedef std::function<void()> CallbackType;

    /**
     * @brief Create Timer Instance (schedule event)
     * @param when[in] Delay (microseconds) before starting the timer. \n
        0 Runs instantly.
     * @param period[in] Cyclic period of the timer (microseconds). \n
        0 Creates One-Shot timer
     * @param callback[in] Function pointer (handler) called when timer is fired.
     * @return TimerId (int64_t) unique identifier for timer
     * @details Timer object is a threaded object scheduling events (callback
        handlers) with microsecond granularity. The tiemr is only as reliable
        as the deployment environment. For greatest precision, please refer to
        the realtime application conditions described here
        (http://www.drdobbs.com/184402031).
     */
    TimerId Create(uint64_t when, uint64_t period, CallbackType callback);

    /**
     * @brief Request timer instance to be deleted.
     * @param id[in] Target timer instance identifier.
     * @return true if successfully deleted. False if timer could not be found or
     *     currenly executing.
     */
    bool Destroy(TimerId id);

    /**
     * @brief Request and wait for timer instance to close and delete.
     * @param id[in] Target timer instance identifier.
     * @return true if successfully deleted. False if timer could not be found.
     */
    bool DestroyWait(TimerId id);

    /**
     * @brief Verify timer instance is running for target identifier.
     * @param id[in] Target timer instance (event) instance.
     * @return true if timer running. False if timer could not be found.
     */
    bool Exists(TimerId id);

    /**
     * @brief Check if Timer Monitor thread is executing.
     * @return true if Timer Monitor thread is running. False if there are no
     *      Timer Instances scheduled.
     * @details Thread is only scheduled to run if a Timer instance has been
     *    scheduled.
     */
    bool Running();

    /**
     * @brief Retrieve cyclic delay for target timer id.
     * @param id[in] Target timer identifier.
     * @return double (microseconds) if Timer found. NAN if timer not found.
     */
    double GetDuration(TimerId id);

    CTimer();
    virtual ~CTimer();

private:
    typedef TInstance<CallbackType, TimerId> TimerInstance;
    typedef std::map<TimerId, TimerInstance*> TimerInstanceMap;
    typedef std::reference_wrapper<TimerInstance> QueueValue;
    typedef SetComparator<TimerInstance> NextActiveComparator;
    typedef std::multiset<TimerInstance*, NextActiveComparator> Queue;

    void Monitor();

    /// Concurrency
    std::mutex              m_mutex;
    std::mutex              m_workerMutex;
    std::condition_variable m_wakeUp;

    /// The Timer
    TimerId                 m_nextId;
    TimerInstanceMap        m_timerMap;
    Queue                   m_queue;

    /// Thread Flags
    std::thread             m_worker;
    volatile sig_atomic_t   m_done;
    volatile sig_atomic_t   m_running;
};

}
}
}

#endif //__OASYS_CORE_COMMON_TIMER_H__
