#include <oasys/core/common/singleton.h>

template <class T>
T *oasys::core::common::TSingleton<T>::GetInstance()
{
    T *pRetVal = m_pInstance.load();
    if (pRetVal == nullptr)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        pRetVal = m_pInstance.load();
        if (pRetVal == nullptr)
        {
            pRetVal = new T;
            m_pInstance.store(pRetVal);
        }
    }

    return pRetVal;
}

template <class T>
bool oasys::core::common::TSingleton<T>::Destroy()
{
    bool retVal = false;

    T *pRetVal = m_pInstance.load();
    if (pRetVal != nullptr)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        pRetVal = m_pInstance.load();
        if (pRetVal != nullptr)
        {
            pRetVal->Destroy();
            delete pRetVal;
            m_pInstance.store(nullptr);
            retVal = true;
        }
    }

    return retVal;
}

template <class T>
oasys::core::common::TSingleton<T>::TSingleton(
        const TSingleton &singleton)
{
    this->m_pInstance.store(singleton.m_pInstance.load());
}

template <class T>
typename oasys::core::common::TSingleton<T>::TSingleton &
    oasys::core::common::TSingleton<T>::operator=(const TSingleton &rhs)
{
    if (this != &rhs)
    {
        this->m_pInstance.store(rhs.m_pInstance.load());
    }

    return *this;
}
