/* This is a managed file. Do not delete this comment. */

#include <include/test.h>

using namespace oasys::core::common;

void test_Timer_TimerCyclic(
    test_Timer _this)
{
    bool retVal = false;

    CTimer timer;
    CTimer::TimerId timerId;
    int execCnt = 0;

    timerId = timer.Create(0, 2,
                                 [&] ()
    {
       execCnt++;
    });

    if (timerId > 0)
    {
       while (execCnt <= 1)
       {
           //spin
       }
       retVal = true;
    }

    if (timer.Running() == true)
    {
       timer.Destroy(timerId);
    }

    test_assert(retVal);
}

void test_Timer_TimerMultiple(
    test_Timer _this)
{
    bool retVal = false;

    CTimer timer;
    CTimer::TimerId timerId1 = 0;
    CTimer::TimerId timerId2 = 0;
    CTimer::TimerId timerId3 = 0;

    timerId1 = timer.Create(0, 100,
                                  [&] ()
    {
    });
    timerId2 = timer.Create(0, 2,
                                  [&] ()
    {
    });
    timerId3 = timer.Create(1, 3,
                                  [&] ()
    {
    });

    while (retVal == false)
    {
        timer.Destroy(timerId1);
        timer.Destroy(timerId2);
        timer.Destroy(timerId3);

        if ((timer.Exists(timerId1) == false) &&
                (timer.Exists(timerId2) == false) &&
                (timer.Exists(timerId3) == false))
        {
            retVal = true;
        }
    }

    test_assert(retVal);
}

void test_Timer_TimerOneShot(
    test_Timer _this)
{
    bool retVal = false;

    CTimer timer;
    CTimer::TimerId timerId;
    int execCnt = 0;

    timerId = timer.Create(0, 0,
                                  [&] ()
    {
        execCnt++;
    });

    if (timerId > 0)
    {
        while (execCnt == 0)
        {
            //spin
        }
        retVal = true;
    }

    if (timer.Running() == true)
    {
        timer.Destroy(timerId);
    }

    test_assert(retVal);
}

void test_Timer_TimerVerify(
    test_Timer _this)
{
    bool retVal = false;

    CTimer timer;
    CTimer::TimerId timerId;
    int execCnt = 0;

    int32_t period = 20;
    timerId = timer.Create(0, period,
                                  [&] ()
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(40));
        execCnt++;
    });

    if (timerId > 0)
    {
        while (execCnt == 0)
        {
            //spin
        }
        if (timer.GetDuration(timerId) > period * 1000)
        {
            retVal = true;
        }
    }

    if (timer.Running() == true)
    {
        timer.Destroy(timerId);
    }

    test_assert(retVal);
}
