#!/bin/bash

GROUP=$2

usage ()
{
    echo "Usage: repo"
    exit
}

buildCorto () {
    rm -rf corto
    git clone git@github.com:cortoproject/corto.git
    cd corto
    source configure && rake && corto install
    cd ..
}

buildProjects () {
    echo $1 "- Setup repositories"
    echo "Remove ${1}"
    rm -rf $1
    git clone git@github.com:cortoproject/${i}.git
    cd ${1}
    corto build && corto install
    cd ..
}

##Define repositories
declare -a repos=(
	"c-binding"
	"json"
	"xml"
	"vm"
    	"ic"
	"corto-language"
	"test"
	"cortodoc"
	"x"
	"web"
	"httprouter"
	"rest"
	"ws"
	"admin"
)

mkdir ../corto_build
cd ../corto_build

buildCorto

for i in "${repos[@]}"
do
    buildProjects $i
done
